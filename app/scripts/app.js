'use strict';

/**
 * @ngdoc overview
 * @name convivaApp
 * @description
 * # convivaApp
 *
 * Main module of the application.
 */
angular
  .module('convivaApp', [
    'ngAnimate',
    'ngRoute',
    'ngSanitize'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'build/views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
