'use strict';

/**
 * @ngdoc function
 * @name convivaApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the convivaApp
 */
angular.module('convivaApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
