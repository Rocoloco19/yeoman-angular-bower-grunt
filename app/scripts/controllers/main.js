'use strict';

/**
 * @ngdoc function
 * @name convivaApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the convivaApp
 */
angular.module('convivaApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
